

# 1st activity
name = "James"
age = 25
work = "developer"
movie_title = "Black Panther"
rating = 95.8

print(f"I am {name}, and I am {age} years old, I work as a {work}, and my rating for {movie_title} is {rating}%")


# 2nd activity

num1, num2, num3 = 5, 12, 9

print(num1 * num2) # product of num1 and num2
print(num1 < num3) # check if num1 is less than num3
print(num3 + num2) # add value of num3 to num2

