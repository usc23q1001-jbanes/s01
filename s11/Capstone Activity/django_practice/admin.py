from django.contrib import admin

# Register your models here.
from .models import ToDoItem, EventItem

admin.site.register(ToDoItem)
admin.site.register(EventItem)