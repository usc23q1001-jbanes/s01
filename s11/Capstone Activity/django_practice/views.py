from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
# from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone
from django.contrib.auth.hashers import make_password

from .models import ToDoItem, EventItem
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, AddEventForm, RegisterForm, ChangePassForm

# Create your views here.

def index(request):
	todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
	eventitem_list = EventItem.objects.filter(user_id=request.user.id)
	context = {
		'todoitem_list': todoitem_list,
		'eventitem_list': eventitem_list,
		'user': request.user
		}

	return render(request, "todolist/index.html", context)

def todoitem(request, todoitem_id):
	todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
	return render(request, 'todolist/todoitem.html', model_to_dict(todoitem))

def eventitem(request, eventitem_id):
	eventitem = get_object_or_404(EventItem, pk=eventitem_id)
	return render(request, 'todolist/event_item.html', model_to_dict(eventitem))


def register(request):

	context = {}
	
	if request.method == 'POST':

		form = 	RegisterForm(request.POST)
		if form.is_valid() == False:
			form = LoginForm()

		else:
			username = form.cleaned_data['username']
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			confirm_password = form.cleaned_data['confirm_password']

			is_user_registered = User.objects.filter(username=username)

			if password == confirm_password:

				if not is_user_registered:
					hash_pass = make_password(password)
					User.objects.create(username=username, first_name=first_name,
					last_name=last_name, email=email, password=hash_pass)

					return redirect("todolist:index")
					
				else:
					context = {
					"error1": True
					}

			else:
				context = {
					"error2": True
					}		


	return render(request, 'register.html', context)

def change_password(request):

	context = {}
	

	if request.method == 'POST':

		form = 	ChangePassForm(request.POST)
		if form.is_valid() == False:
			form = ChangePassForm()

		else:
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			password = form.cleaned_data['password']
			username = request.user.username

			is_user_registered = User.objects.filter(username=username)

			if not is_user_registered:
				context = {
					"error": True
					}
			else:
				user = User.objects.get(username=username)
				# hash_pass = make_password(password)
				user.first_name = first_name
				user.last_name = last_name
				user.set_password(password)
				user.save()	
				return redirect("todolist:index")		


	return render(request, 'change_password.html', context)

def login_view(request):
	context = {}

	if request.method == 'POST':
		form = LoginForm(request.POST)

		if form.is_valid() == False:
			form = LoginForm()

		else:
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user = authenticate(username=username, password=password)
			context = {
				"username": username,
				"password": password
			}
			if user is not None:
				login(request, user)
				return redirect('todolist:index')

			else: 
				context = {
					"error": True
				}

	return render(request, 'login.html', context)
	
def logout_view(request):
	logout(request)
	return redirect('todolist:index')

def add_task(request):

	context = {}

	if request.method == 'POST':
		form = AddTaskForm(request.POST)
		if form.is_valid() == False:
			
			form = AddTaskForm()

		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']

			duplicates = ToDoItem.objects.filter(task_name=task_name)

			if not duplicates:
				ToDoItem.objects.create(task_name=task_name, description=description, 
					date_created=timezone.now(), user_id=request.user.id)
				return redirect("todolist:index")

			else:
				context = {
					"error": True
				}

	return render(request, "todolist/add_task.html", context)

def update_task(request, todoitem_id):
	todoitem = ToDoItem.objects.filter(pk=todoitem_id)

	context = {
		"user": request.user,
		"todoitem_id": todoitem_id,
		"task_name": todoitem[0].task_name,
		"description": todoitem[0].description,
		"status": todoitem[0].status
	}

	if request.method == 'POST':
		form = UpdateTaskForm(request.POST)
		if form.is_valid() == False:
			form = UpdateTaskForm()

		else: 
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if todoitem:
				todoitem[0].task_name = task_name
				todoitem[0].description = description
				todoitem[0].status = status	

				todoitem[0].save()
				return redirect("todolist:index")

			else:
				context = {
					"error": True
				}
	return render(request, "todolist/update_task.html", context)

def delete_task(request, todoitem_id):
	todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()
	return redirect("todolist:index")

def add_event(request):

	context = {}

	if request.method == 'POST':
		form = AddEventForm(request.POST)
		if form.is_valid() == False:
			
			form = AddEventForm()

		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']

			duplicates = EventItem.objects.filter(event_name=event_name)

			if not duplicates:
				EventItem.objects.create(event_name=event_name, description=description, 
					event_date=timezone.now(), user_id=request.user.id)
				return redirect("todolist:index")

			else:
				context = {
					"error": True
				}

	return render(request, "todolist/add_event.html", context)