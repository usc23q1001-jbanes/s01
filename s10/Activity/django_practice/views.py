from django.shortcuts import render, redirect
from django.http import HttpResponse
# from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.contrib.auth.hashers import check_password, make_password


from .models import GroceryItem
from .forms import LoginForm, RegisterForm

# Create your views here.

def index(request):
	groceryitem_list = GroceryItem.objects.all()
	# groceryitem_list = GroceryItem.objects.filter(user_id=request.user.id)
	context = {
		'groceryitem_list': groceryitem_list,
		'user': request.user
		}

	return render(request, "index.html", context)

def groceryitem(request, groceryitem_id):
	groceryitem = model_to_dict(GroceryItem.objects.get(pk=groceryitem_id))
	return render(request, 'groceryitem.html', groceryitem)


def register(request):

	context = {}
	
	if request.method == 'POST':

		form = 	RegisterForm(request.POST)
		if form.is_valid() == False:
			form = LoginForm()

		else:
			username = form.cleaned_data['username']
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			confirm_password = form.cleaned_data['confirm_password']

			is_user_registered = User.objects.filter(username=username)

			if password == confirm_password:

				if not is_user_registered:
					hash_pass = make_password(password)
					User.objects.create(username=username, first_name=first_name,
					last_name=last_name, email=email, password=hash_pass)

					return redirect("grocerylist:index")
					
				else:
					context = {
					"error1": True
					}

			else:
				context = {
					"error2": True
					}		


	return render(request, 'register.html', context)

def change_password(request):
	is_user_authenticated = False

	user = authenticate(username='johndoe', password='john1234')
	print(user)

	if user is not None:
		authenticated_user = User.objects.get(username='johndoe')
		authenticated_user.set_password('johndoe1')
		authenticated_user.save()
		is_user_authenticated = True

	context = {
		'is_user_authenticated': is_user_authenticated
	}

	return render(request, 'change_password.html', context)


def login_view(request):
	context = {}

	if request.method == 'POST':
		form = LoginForm(request.POST)

		if form.is_valid() == False:
			form = LoginForm()

		else:
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user = authenticate(username=username, password=password)
			context = {
				"username": username,
				"password": password
			}
			if user is not None:
				login(request, user)
				return redirect('grocerylist:index')

			else: 
				context = {
					"error": True
				}
	return render(request, 'login.html', context)

def logout_view(request):
	logout(request)
	return redirect('grocerylist:index')
