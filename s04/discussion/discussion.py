# Python has several structures to store collections or multiple items in a single variable

# Lists
# Lists are similar to arrays in JavaScript in a sense that they can contain a collection of data
# To create a list, the square bracket([]) is used.

names = ["John", "Paul", "George", "Ringo"]
programs = ['developer career', 'pi-shape', 'short courses']

durations = [260, 180, 20]
truth_values = [True, False, True, True, False]

# A list can contain elements of different data types
sample_list = ["Apple", 3, False, "Potato", 4, True]

print(sample_list)

# Getting the list size 
print(len(programs))

# Accessing values
# List can be accessed by providing the index number of the element 

# Access the first item in the list 
print(programs[0]) #developer career

# Access the last item in the list
print(programs[-1]) #short courses

# Access the second item in the list 
print(durations[1]) #180

# Access the whole list
print(durations) # [260,180,20]

# Access a range of values
# list [start index: end index]
print(programs[0:2]) # ['developer career', 'pi-shape']

# Updating lists
# Print the current value
print(f'Current value: {programs[2]}')

# Update the value
programs[2] = 'Short Courses'

# Print the new value
print(f'New value: {programs[2]}')

# Mini-exercise
# 1. Create a list of names of 5 students
# 2. Create a list of grades for the 5 students
# 3. Use a loop to iterate through the list printing in the following format: 
# The grade of John is 100

students = ['Ross', 'Chandler', 'Joey', 'Monica', 'Rachel']
grades = [100, 92, 75, 93, 80]

x = 0
while x < len(students):
	print(f"The grade of {students[x]} is {grades[x]}")
	x += 1 

# Adding List item - the append() method allows to insert items to a list
programs.append('global')
print(programs)

# Deleting List items
# Add new item to the duration list
durations.append(360)
print(durations)

# Delete the last item on the list
del durations[-1]
print(durations)

# Membership checks - the "in" keyword checks if the elements is in the list 
# returns true or false
print(20 in durations)
print(500 in durations)

# Sorting lists - the sort() method sorts the list alphanumerically, ascending by default
print(names)
names.sort()
print(names)

# Emptying the list - the clear() method is used to empty the contents of the list

test_list = [1,3,5,7,9]
print(test_list)
test_list.clear()
print(test_list)

# Dictionaries are used to store data values in key:value pairs
# To create a dictionary, the curly braces({}) is used and the key-value pairs are denoted with (key:value)
person1 = {
	"name": "Nikolai",
	"age": 18,
	"occupation": "Student",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Django"]
}

print(person1)

# To get the number of key-value pairs in the dictionary, the len() method can be used
print(len(person1))


# Accessing values in the dictionary
print(person1["name"])

# The keys() method will return a list of all the keys in the dictionary
print(person1.keys())

# The values() method will return a list of all the values in the dictionary
print(person1.values())

# The items() method will return each item in a dictionary as a key-value pair
print(person1.items())

# Adding key-value pairs can be done either putting a new index key and assigning a value or the update() method
person1["nationality"] = "Filipino"
person1.update({"fave_food": "Sinigang"})
print(person1)

# clear method empties the dictionary
person2 = {
	"name" : "John",
	"age" : 25
}

print(person2)
person2.clear()
print(person2)



# Mini-Exercise
# 1. Create a car dictionary with the following keys
# brand, model, year of make, color
# 2. Print the following statement from the the details:
# "I own a <brand> <model> and it was made in <year of make>"

car1 = {
	"brand": "Nissan",
	"model": "GT-R",
	"year of make": 2020,
	"color": "Blue"
}

print(car1)
print(f"I own a {car1['brand']} {car1['model']} and it was made in {car1['year of make']}")


















