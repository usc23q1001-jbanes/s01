from abc import ABC, abstractclassmethod

class Animal(ABC):

	def __init__(self, name, breed, age):
		self.name = name
		self.breed = breed
		self.age = age

	@abstractclassmethod

	def eat(self, food):
		pass

	def make_sound(self):
		pass


class Dog(Animal):

	def __init__(self, name, breed, age):
		super().__init__(name, breed, age)

	def eat(self, food):
		print(f'The dog has eaten {food}.')

	def make_sound(self):
		print(f'Bark! Woof! Arf!')

	def call(self):
		print(f'Here {self.name}! ')


class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__(name, breed, age)

	def eat(self, food):
		print(f'Serve me {food}.')

	def make_sound(self):
		print(f'Meow! Nyaw! Nyaaaaa!')

	def call(self):
		print(f'{self.name}, come on!')


dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()


cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()