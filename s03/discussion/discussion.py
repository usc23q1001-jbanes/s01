
# username = input("Enter the name: \n")
# print(f"Hi {username}! Welcome to this course.")


# num1 = int(input("Enter 1st number: \n"))
# num2 = int(input("Enter 2nd number: \n"))
# print(f"The sum of num1 and num2 is {num1 + num2}")

#If-else statements

# test_num = 50

# if test_num >= 60:
# 	print("Test Passed.")
# else: 
# 	print("Test Failed.")


# Else-if chains

# test_num2 = int(input("Please enter 2nd test number: \n"))

# if test_num2 > 0: 
# 	print("The number is positive")
# elif test_num2 == 0:
# 	print("The number is zero")
# else: 
# 	print("The number is negative")

# Mini-exercise:
# Create an if-else statement that determines if a number is divisible by 3, 5, or both
# If the number is divisible by 3, print "The number is divisible by 3"
# If the number is divisible by 5, print "The number is divisible by 5"
# If the number is divisible by 3 and 5, print "The number is divisible by both 3 and 5"
# If the number is not divisible by any, print "The number is not divisible by 3 nor 5"

# test_div = int(input("Enter number: \n"))

# if test_div % 3 == 0 and test_div % 5 == 0:
# 	print("The number is divisible by both 3 and 5")
# elif test_div % 3 == 0:
# 	print("The number is divisible by 3")
# elif test_div % 5 == 0:
# 	print("The number is divisible by 5")
# else:
# 	print("The number is not divisible by 3 nor 5")

i = 1 
while i <= 5:
	print(f"Current Count {i}")
	i += 1

# fruits = ["apple", "banana","cherry"]
# for indiv_fruit in fruits:
# 	print(indiv_fruit)



# for x in range(6,10):
# 	print(f"The current value is {x}")


# j = 1 
# while j < 6:
# 	print(j)
# 	if j == 3:
# 		break
# 	j +=1