
year = int(input("Please input a year: "))

if year <= 0:
	print(f"{year} is not a leap year")
elif year % 4 == 0:
	print(f"{year} is considered a leap year")


row = int(input("Enter number of rows: "))
col = int(input("Enter number of columns: "))

while row > 0:
	x = col;
	while x > 0:
		print("*", end = " ")
		x -= 1
	print(' ')
	row -=1

	